﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BejoLib;

namespace ModernUI
{
    /// <summary>
    /// Interaction logic for RulesWindow.xaml
    /// </summary>
    public partial class RulesWindow : Window
    {
        public RulesWindow()
        {
            InitializeComponent();

            BejoCorrectionRule[] basicRulesWorkflow = Correction.GetBasicWorkflowRules;
            BejoCorrectionRule[] extendedRulesWorkflow = Correction.GetExtendedWorkflowRules;

            DisplayRule[] displayBasicRulesWorkflow 
                = (from ii in extendedRulesWorkflow
                   select new DisplayRule
                   { Name = ii.ToString().Replace("BejoLib.",""),
                     Description = ii.Description,
                     Status = ii.Staus.ToString(),
                     IsPartOfBasicWorkflow = basicRulesWorkflow.Contains(ii) ? "Yes" : "No",
                     IsPartOfExtendedflow = "Yes",
                   }).ToArray();

            lvLeft.ItemsSource = displayBasicRulesWorkflow;
        }

        /// <summary>
        /// Represents the rule for the UI
        /// </summary>
        class DisplayRule
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public string Status { get; set; }
            public string IsPartOfBasicWorkflow { get; set; }
            public string IsPartOfExtendedflow { get; set; }
        }
    }
}
