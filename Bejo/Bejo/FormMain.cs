﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Text.RegularExpressions;
using BejoLib;
using System.IO;

namespace Bejo
{
    public partial class FormMain : Form
    {
        BackgroundWorker worker;

        BejoText text;

        public FormMain()
        {
            InitializeComponent();
            this.CenterToScreen();

            worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += Worker_DoWork;
            worker.ProgressChanged += Worker_ProgressChanged;
            worker.RunWorkerCompleted += Worker_RunWorkerCompleted;

            this.Text = "Bejo " + Helper.BejoVersion + " - French Language Word Processor ";
            tbStaus.Text = "Ready.";

            textBoxInputText.Text = BejoLib.BejoDocumentDB.GetDocumentDemo();
        }

        private void Correction_TaskCompleted(int completed)
        {
            worker.ReportProgress(completed);
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show("The following error has been detected: " + e.Error.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                textBoxOutputText.Text = text.GetModifiedText();

                BejoWord[] taggedWords = text.GetTaggedWords;

                //Underline
                foreach (var word in taggedWords)
                {
                    if (word.IsCorrected)
                    {
                        textBoxOutputText.SelectionStart = word.CorrectionPosition;
                        textBoxOutputText.SelectionLength = word.CorrectionLength;
                        textBoxOutputText.SelectionFont = new Font(textBoxOutputText.SelectionFont, FontStyle.Underline);

                        //disable underlining for next character
                        textBoxOutputText.SelectionStart = word.CorrectionPosition + word.CorrectionLength;
                        textBoxOutputText.SelectionLength = 1;
                        textBoxOutputText.SelectionFont = new Font(textBoxOutputText.SelectionFont, FontStyle.Regular);
                    }
                }

                progressBar.Value = 100;

                tbStaus.Text = "Done.";
            }

            BejoLib.Correction.TaskCompleted -= Correction_TaskCompleted;

            buttonProcess.Enabled = true;
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            text = Correction.Execute(textBoxInputText.Text, true);
        }

        private void buttonProcess_Click(object sender, EventArgs e)
        {
            textBoxOutputText.Text = "";

            if (radioButtonPOS.Checked)
            {
                string posText = POS.GetTaggedText(textBoxInputText.Text);

                textBoxOutputText.Text = posText;
            }
            else
            if (radioButtonCorrection.Checked)
            {
                BejoLib.Correction.TaskCompleted += Correction_TaskCompleted;
                worker.RunWorkerAsync();
                buttonProcess.Enabled = false;
                progressBar.Value = 5;
                tbStaus.Text = "Processing ... please wait. It might take longer for bigger texts.";
            }
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            BejoDictionary.ImportLefff(Helper.GetDefaultDictionaryPath());

            POS.Initialize();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void dictionaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormDictionary d = new FormDictionary();
            d.Show();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAbout fa = new FormAbout();
            fa.Show();
        }
    }
}
