﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using BejoLib;

namespace Bejo
{
    public partial class FormDictionary : Form
    {
        public FormDictionary()
        {
            InitializeComponent();
            this.CenterToScreen();

            dataGridView1.AutoGenerateColumns = true;
            dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.Fill);
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.ReadOnly = true;

            label2.Text = BejoLib.BejoDictionary.BejoWordDictionary.Count.ToString();

            textBox1.KeyDown += new KeyEventHandler(textBox1_KeyDown);
        }

        void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                buttonSearch_Click(null, null);
                e.Handled = true;
            }
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            string word = textBox1.Text.ToLower();

            BejoDictionaryWord[] results = BejoLib.BejoDictionary.BejoWordDictionary.AsParallel().Where(x => x.Content.Equals(word, StringComparison.OrdinalIgnoreCase)).ToArray();

            if (results.Length == 0)
            {
                results = (from w in BejoDictionary.BejoWordDictionary.AsParallel()
                            let x = w.Content.ToLower().Replace('é', 'e').Replace('è', 'e')
                            where x == word
                            select w).ToArray();
            }

            DataTable dt = new DataTable();

            dt.Columns.Add("Value");
            dt.Columns.Add("Type");
            dt.Columns.Add("Gender");
            dt.Columns.Add("Number");

            foreach (var r in results)
            {
                dt.Rows.Add(new object[] { r.Content,r.Type,r.Gender, r.Number });
            }

            dataGridView1.DataSource = dt;
        }
    }
}
