﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

<title>Bejo - a grammar and spell-checker for French</title>

<!-- For mobile devices -->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<meta name="description" content="Bejo is able to capture some of the basic rules of French grammar. It checks your e-mails for: gender validation over nouns and adjectives, singular vs plural and more." />

<!-- Twitter Card data -->
<meta name="twitter:card" value="Bejo - a grammar and spell-checker for French">

<!-- Open Graph data -->
<meta property="og:site_name" content="Bejo"/>
<meta property="og:title" content="Bejo - a grammar and spell-checker for French" />
<meta property="og:type" content="website" />
<meta property="og:url" content="http://bejo.micromedia.bg" />
<meta property="og:image" content="http://bejo.micromedia.bg/Images/marianne1-big.jpg" />
<meta property="og:description" content="Bejo is able to capture some of the basic rules of French grammar. It checks your e-mails for: gender validation over nouns and adjectives, singular vs plural and more." />

<!-- Google Analytics -->
<script>
window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
ga('create', 'UA-91135457-1', 'auto');
ga('send', 'pageview');
</script>
<script async src='https://www.google-analytics.com/analytics.js'></script>
<!-- End Google Analytics -->

<style>

        html *
        {
           font-size:medium;
           font-family: "Times New Roman", Times, serif;
        }

        .button-success,
        .button-error,
        .button-warning,
        .button-secondary {
            color: white;
            border-radius: 4px;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
        }

         .button-success {
            font-size: 110%;
            background: rgb(28, 184, 65); /* this is a green */
            padding: 5px 8px 5px 8px;
        }

        table.products
        {
            width: 100%;
            min-width: 160px;
            table-layout: fixed;
        }

        
         table.products td.price {
            text-align: center;
         }

         table.products td.sell {
            text-align: right;
         }

         table.products td.buy {
            text-align: left;
         }

        textarea[name=tbInput] { border: thin solid #00CC99; width:100%; height:100%; min-height:100px;}
        
        div[name=lbMessage] { color: darkorange;  font-size: 130%;}

        div[name=tbOutput] { border: thin solid #00CC99; width:100%; height:100%; min-height:100px;}

        img.resize
        {
           max-width:50%;
           max-height:50%;
        }

        /*Share buttons CSS*/
        .resp-sharing-button__link,
        .resp-sharing-button__icon {
          display: inline-block
        }

        .resp-sharing-button__link {
          text-decoration: none;
          color: #fff;
          margin: 0.1em
        }

        .resp-sharing-button {
          border-radius: 5px;
          transition: 25ms ease-out;
          padding: 0.12em 0.18em;
          font-family: Helvetica Neue,Helvetica,Arial,sans-serif
        }

        .resp-sharing-button__icon svg {
          width: 1em;
          height: 1em;
          margin-right: 0.4em;
          vertical-align: top
        }

        .resp-sharing-button--small svg {
          margin: 0;
          vertical-align: middle
        }

        /* Non solid icons get a stroke */
        .resp-sharing-button__icon {
          stroke: #fff;
          fill: none
        }

        /* Solid icons get a fill */
        .resp-sharing-button__icon--solid,
        .resp-sharing-button__icon--solidcircle {
          fill: #fff;
          stroke: none
        }

        .resp-sharing-button--twitter {
          background-color: #55acee
        }

        .resp-sharing-button--twitter:hover {
          background-color: #2795e9
        }

        .resp-sharing-button--facebook {
          background-color: #3b5998
        }

        .resp-sharing-button--facebook:hover {
          background-color: #2d4373
        }

        .resp-sharing-button--linkedin {
          background-color: #0077b5
        }

        .resp-sharing-button--linkedin:hover {
          background-color: #046293
        }

        .resp-sharing-button--facebook {
          background-color: #3b5998;
          border-color: #3b5998;
        }

        .resp-sharing-button--facebook:hover,
        .resp-sharing-button--facebook:active {
          background-color: #2d4373;
          border-color: #2d4373;
        }

        .resp-sharing-button--twitter {
          background-color: #55acee;
          border-color: #55acee;
        }

        .resp-sharing-button--twitter:hover,
        .resp-sharing-button--twitter:active {
          background-color: #2795e9;
          border-color: #2795e9;
        }

        .resp-sharing-button--linkedin {
          background-color: #0077b5;
          border-color: #0077b5;
        }

        .resp-sharing-button--linkedin:hover,
        .resp-sharing-button--linkedin:active {
          background-color: #046293;
          border-color: #046293;
        }

</style>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 

<script>
        var urlToHandler = 'ProcessHandler.ashx';

        $(document).ready(function () {

            $('#imgLoading').hide();

            $("#buttonClearInputText").click(function () {

                $("#lbMessage").html('');//clear old message
                $("#tbInput").val('');
            });

            $("#buttonLoadDemoText").click(function () {

                $("#lbMessage").html('');//clear old message
                $('#imgLoading').show();

                Promise.resolve(
                         $.ajax({
                             url: urlToHandler,
                             data: '{ "Command": "GetDemoDocument", "TextToCorrect": ""}',
                             dataType: 'json',
                             type: 'POST',
                             contentType: 'application/json',
                             timeout: 30000 //30 seconds timeout
                         })
                       ).then(function (data) {

                           $('#imgLoading').hide();
                           $("#tbInput").val(data.CorrectedText);

                       }).catch(function (e) {

                           $('#imgLoading').hide();

                           console.log(e);
                           if (e.statusText == 'timeout') {
                               $("#lbMessage").html('Error: failed timeout!');
                           } else
                               if (e == 'Internal server error!') {
                                   $("#lbMessage").html('Server error!'); //problem in C#
                               } else
                                   if (!IsJsonString(e.responseText)) {
                                       $("#lbMessage").html('Error: invalid JSON returned!');
                                   }
                                   else {
                                       $("#lbMessage").html('There was an error!');
                                   }
                       })
            });

            //attach a click event to button "Process"
            $("#buttonProcess").click(function () {

                $("#lbMessage").html('');//clear old message
                var TextToCorrect = $("#tbInput").val();

                //validate input
                if (!/^[\s\S]{3,2000}$/.test(TextToCorrect))
                {
                    $("#lbMessage").html('Error: Incorrect input text!');
                    $("#tbOutput").html("Not available.");
                    return;
                }

                $('#imgLoading').show();
                $("#tbOutput").html("Processing ...");
                
                jsonData = '{ "Command": "ApplyCorrection", "TextToCorrect": ' + JSON.stringify(TextToCorrect) + '}';

                Promise.resolve(
                      $.ajax({
                              url: urlToHandler,
                              data: jsonData,
                              dataType: 'json',
                              type: 'POST',
                              contentType: 'application/json',
                              timeout: 30000 //30 seconds timeout
                      })
                    ).then(function (data)
                    {
                        $('#imgLoading').hide();

                        if (data.CorrectedText != "Error") {

                            if (data.numbers.length > 0) //corrections available
                            {
                                var coloredText = addColor(data.CorrectedText, data.numbers);
                                var newLinesText = addNewLine(coloredText);
                                $("#tbOutput").html(newLinesText);
                            }
                            else {
                                $("#lbMessage").html('<font color=green>No correction applied.</font>');
                                var newLinesText = addNewLine(data.CorrectedText);
                                $("#tbOutput").html(newLinesText);
                            }
                        } else {
                            throw "Internal server error!"; //problem in C#
                        }
                    }).catch(function (e) {

                        console.log(e);
                        $('#imgLoading').hide();

                       
                        if (e.statusText == 'timeout') {
                            $("#lbMessage").html('Error: failed timeout!');
                        }
                        else
                        if (e == 'Internal server error!') {
                            $("#lbMessage").html('Server error!'); //problem in C#
                        }
                        else
                        if (!IsJsonString(e.responseText)) {
                            $("#lbMessage").html('Error: invalid JSON returned!');
                        }
                        else
                        {
                            $("#lbMessage").html('There was an error!');
                        }

                        $("#tbOutput").html("Not available.");
                    });         
            });//end event button click
 
            document.getElementById("buttonCopy").addEventListener("click", function () {
                copyToClipboard(document.getElementById("tbOutput"));
            });

        });//end document ready
       
        function addColor(correctedText, numbers) //numbers: position, length , position, length ...
        {
            //alert("addColor");
            last = 0;
            var coloredText = "";

            for (i = 0; (i+1) < numbers.length; i+=2) {
                
                //alert(i);

                coloredText += correctedText.substring(last, numbers[i]);//between last and position
                //alert(coloredText);

                var word = correctedText.substring(numbers[i], numbers[i] + numbers[i + 1]);//postion + length
                //alert(word);

                coloredText += "<FONT COLOR=\"red\">" + word + "</FONT>";
                //alert(coloredText);

                last = numbers[i] + numbers[i + 1];
                //alert(last);
            }

            coloredText += correctedText.substring(last, correctedText.length);
            return coloredText;
        }

        function addNewLine(correctedText) //replaces "\n" with "<br />"
        {
            last = 0;
            var textNewLines = "";

            for (j = 0; j < correctedText.length; j++) {

                if (correctedText[j] == "\n") {

                    textNewLines += correctedText.substring(last, j);
                    textNewLines += "<br />";
                    last = j + 1;                  
                }
                //else if ((j-1)>=0 && correctedText[j] == " " && (correctedText[j-1] == "\n" || correctedText[j-1] == " ")) {

                //    textNewLines += "&nbsp;";
                //    last = j + 1;
                //}
            }

            textNewLines += correctedText.substring(last, correctedText.length);
            return textNewLines;
        }

        function IsJsonString(str) {
            try {
                JSON.parse(str);
            }
            catch (e) {
                return false;
            }
            return true;
        }

        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput)
            {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;

            } else
            {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch (e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }
    </script>
</head>

<body>
    <form id="form1" runat="server">
        <div style="max-width:800px; margin:0 auto;">
        <table class="products" >
            <tr><td colspan="3" class="price"><span style="font-weight:bold;font-size:x-large">Bejo - a grammar checker for French</span></td></tr>
            <tr>
                <td colspan="3" class="price">
                <img src="Images/marianne1.jpg" alt="Image result for marianne la republique francaise"/>
                </td>
            </tr>
            <tr>
                <td colspan="3">Bejo is able to capture some of the basic rules of French grammar. It checks your e-mails for: gender validation over nouns and adjectives, singular vs plural and <a href="http://www.lepointdufle.net/ressources_fle/articles_de_du2.htm" target="_blank">articles contractés</a>.</td>
            </tr>
            <tr>
                <td colspan="3" class="sell">
                    <button type="button" id="buttonLoadDemoText" class="button-success pure-button">Load demo text</button>
                    &nbsp;&nbsp;
                    <button type="button" id="buttonClearInputText" class="button-success pure-button">Clear text</button>
                </td>
            </tr>
            <tr>
                <td colspan="3">Enter french text:</td>
            </tr>
            <tr>
                <td colspan="3">
                    <textarea id="tbInput" name="tbInput"></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button type="button" id="buttonProcess" class="button-success pure-button">Process</button>
                    &nbsp; <div style="display: inline" id="lbMessage" name="lbMessage" />
                </td>
                <td style="height: 50px; text-align: right;">
                    <img id="imgLoading" src="Images/ajax-loader.gif" width="46" height="46"/>
                </td>
            </tr>
            <tr>
                <td colspan="3">Corrected text:</td>
            </tr>
            <tr>
                 <td colspan="3">
                     <div id="tbOutput" name="tbOutput">
                     </div>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="sell">
                    <button type="button" id="buttonCopy" class="button-success pure-button">Copy result to clipboard</button>
                </td>
            </tr>
            <tr>
                 <td colspan="2" class="buy">
                   
                    <!-- Sharingbutton Facebook -->
                    <a class="resp-sharing-button__link" href="https://facebook.com/sharer/sharer.php?u=http%3A%2F%2Fbejo.micromedia.bg" target="_blank" aria-label="">
                      <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.77 7.46H14.5v-1.9c0-.9.6-1.1 1-1.1h3V.5h-4.33C10.24.5 9.5 3.44 9.5 5.32v2.15h-3v4h3v12h5v-12h3.85l.42-4z"/></svg>
                        </div>
                      </div>
                    </a>

                    <!-- Sharingbutton Twitter -->
                    <a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet/?text=&amp;url=http%3A%2F%2Fbejo.micromedia.bg" target="_blank" aria-label="">
                      <div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M23.44 4.83c-.8.37-1.5.38-2.22.02.93-.56.98-.96 1.32-2.02-.88.52-1.86.9-2.9 1.1-.82-.88-2-1.43-3.3-1.43-2.5 0-4.55 2.04-4.55 4.54 0 .36.03.7.1 1.04-3.77-.2-7.12-2-9.36-4.75-.4.67-.6 1.45-.6 2.3 0 1.56.8 2.95 2 3.77-.74-.03-1.44-.23-2.05-.57v.06c0 2.2 1.56 4.03 3.64 4.44-.67.2-1.37.2-2.06.08.58 1.8 2.26 3.12 4.25 3.16C5.78 18.1 3.37 18.74 1 18.46c2 1.3 4.4 2.04 6.97 2.04 8.35 0 12.92-6.92 12.92-12.93 0-.2 0-.4-.02-.6.9-.63 1.96-1.22 2.56-2.14z"/></svg>
                        </div>
                      </div>
                    </a>

                    <!-- Sharingbutton LinkedIn -->
                    <a class="resp-sharing-button__link" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=http%3A%2F%2Fbejo.micromedia.bg&amp;title=&amp;summary=&amp;source=http%3A%2F%2Fbejo.micromedia.bg target="_blank" aria-label="">
                      <div class="resp-sharing-button resp-sharing-button--linkedin resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M6.5 21.5h-5v-13h5v13zM4 6.5C2.5 6.5 1.5 5.3 1.5 4s1-2.4 2.5-2.4c1.6 0 2.5 1 2.6 2.5 0 1.4-1 2.5-2.6 2.5zm11.5 6c-1 0-2 1-2 2v7h-5v-13h5V10s1.6-1.5 4-1.5c3 0 5 2.2 5 6.3v6.7h-5v-7c0-1-1-2-2-2z"/></svg>
                        </div>
                      </div>
                    </a>

                </td>
                <td class="sell"><a href="https://bitbucket.org/toncho11/bejo" target="_blank">Project homepage</a></td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
