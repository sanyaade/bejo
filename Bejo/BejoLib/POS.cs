﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using java.io;
using java.util;
using edu.stanford.nlp.ling;
using edu.stanford.nlp.tagger.maxent;

//corenlp
using edu.stanford.nlp.pipeline;
using Console = System.Console;
using edu.stanford.nlp.trees;
using edu.stanford.nlp.process;
using edu.stanford.nlp.parser.lexparser;

using System.Globalization;
using System.Threading;

namespace BejoLib
{
    /// <summary>
    /// POS - Part of speech tagger. Assigns to each word in text its part of speech class: noun, adjective, verb, etc.
    /// </summary>
    public static class POS
    {
        private static MaxentTagger tagger;

        private static bool isInitialized = false;

        static POS()
        {
            Initialize();
        }

        public static void Initialize()
        {
            if (!isInitialized)
            {
                var modelsDirectory = Helper.GetNLPModelsFolder();

                //changing culture to avoid java exceptions
                CultureInfo ci = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentCulture = ci;
                Thread.CurrentThread.CurrentUICulture = ci;

                // Loading POS Tagger
                tagger = new MaxentTagger(modelsDirectory + System.IO.Path.DirectorySeparatorChar + "french.tagger");
                isInitialized = true;
            }
        }

        /// <summary>
        /// Returns a list of tagged sentences. Used by the Parser.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static List<List> GetTaggedSentences(string text)
        {
            if (!isInitialized) throw new Exception("POS Tagger not initalized!");

            var sentences = MaxentTagger.tokenizeText(new StringReader(text)).toArray();

            List <List> taggedSentences = new List<List>();

            foreach (ArrayList sentence in sentences)
            {
                List taggedSentence = tagger.tagSentence(sentence);
                taggedSentences.Add(taggedSentence);
            }

            return taggedSentences;
        }

        /// <summary>
        /// Returns a list of tagged sentences. Each sentence is a list of BejoWord items.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static System.Collections.Generic.List<System.Collections.Generic.List<BejoWord>> GetTaggedSentencesBejoWordFormat(string text)
        {
            if (!isInitialized) throw new Exception("POS Tagger not initalized!");

            var sentences = MaxentTagger.tokenizeText(new StringReader(text)).toArray();

            System.Collections.Generic.List<System.Collections.Generic.List<BejoWord>> allTaggedSentences = new List<List<BejoWord>>();

            foreach (ArrayList sentence in sentences)
            {
                List taggedSentence = tagger.tagSentence(sentence);

                allTaggedSentences.Add(ConvertJavaTWordToDotnetTwordList(taggedSentence));
            }

            FixTaggerErrors(ref allTaggedSentences);

            return allTaggedSentences;
        }

        /// <summary>
        /// Outputs both the text and POS's tag for each word in a single string.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string GetTaggedText(string text)
        {
            string result = "";

            System.Collections.Generic.List<System.Collections.Generic.List<BejoWord>> taggedList = GetTaggedSentencesBejoWordFormat(text);

            for (int j = 0; j < taggedList.Count; j++)
            {
                var sentence = taggedList[j];

                for (int i = 0; i < sentence.Count; i++)
                {
                    result+= sentence[i].POSTaggedWord.value() + "/" + sentence[i].POSTaggedWord.tag() + " ";
                }
            }

            if (result.Length > 1)
                result = result.Substring(0, result.Length - 1);

            return result;
        }

        public static string JavaListToString(ArrayList sentences)
        {
            string result = "";

            foreach (ArrayList sentence in sentences)
            {
                result += Sentence.listToString(sentence, false);
            }

            return result;
        }

        private static List<BejoWord> ConvertJavaTWordToDotnetTwordList(List jlist)
        {
            List<BejoWord> result = new List<BejoWord>();

            for (int i = 0; i < jlist.size(); i++)
            {
                TaggedWord tword = (TaggedWord)jlist.get(i);
                result.Add(new BejoWord(tword));
            }

            return result;
        }

        private static void FixTaggerErrors(ref List<List<BejoWord>> allSentences)
        {
            int corrected = 0;

            for (int j = 0; j < allSentences.Count; j++)
            {
                var sentence = allSentences[j];

                for (int i = 0; i < sentence.Count; i++)
                {
                    if (i + 1 < sentence.Count)
                    {
                        TaggedWord tword1 = sentence[i].POSTaggedWord;
                        TaggedWord tword2 = sentence[i + 1].POSTaggedWord;

                        //Correction 1: tword1 DET -> ADJ
                        if (tword1.tag() == "DET" && tword2.tag().StartsWith("N")
                            && (tword1.value().ToLower() == "ce" || tword1.value().ToLower() == "cet" || tword1.value().ToLower() == "cette" || tword1.value().ToLower() == "ces")
                            )
                        {
                            sentence[i].POSTaggedWord.setTag("ADJ");
                            corrected++;
                        }

                        //Correction 2: La personne est venue. 
                        //"personne" is not a PRO, but a N
                        //if (tword1.tag() == "DET" && tword2.tag().Equals("PRO"))
                        //    sentence[i+1].POSTaggedWord.setTag("N");
                    }
                }
            }

            bool enableEwordsTypeFix = true;
            //Change if we are sure that there is a mistake because of the "e"s in the tagging for an adj or noun
            //ex. "Je suis désolé" is covered correction below.
            if (enableEwordsTypeFix)
            {
                for (int j = 0; j < allSentences.Count; j++)
                {
                    var sentence = allSentences[j];

                    for (int i = 0; i < sentence.Count; i++)
                    {
                        TaggedWord tword = sentence[i].POSTaggedWord;

                        if (tword.value().Contains("e")
                            && (tword.tag() == "ADJ" || tword.tag().StartsWith("N")))
                        {
                            string twordAllESame = BejoNLPhelper.ConvertToLowerAllE(tword.value());

                            //The suggestions are identical to source tagged word
                            var suggestions = BejoDictionary.BejoWordDictionary.AsParallel().Where(x => (x.Type == BejoWordType.Noun || x.Type == BejoWordType.Adjective) && x.AllESame.Equals(twordAllESame)).ToArray();

                            //remove duplicates (ex. "désolé")
                            suggestions = suggestions.GroupBy(x => x.Content).Select(x => x.First()).ToArray();

                            bool typeIsEqual = suggestions.Length > 0
                                                &&
                                                (
                                                    (tword.tag() == "ADJ" && suggestions[0].Type == BejoWordType.Adjective)
                                                    ||
                                                    (tword.tag().StartsWith("N") && suggestions[0].Type == BejoWordType.Noun)
                                                );

                            //If the suggestion matches the input word, then there is no error 
                            //They should not match over the "e"s - this gives a chance for error in the tagging
                            bool inputWordIsDifferentFromDictionary = suggestions.Length == 1 && suggestions[0].Content != tword.value();

                            //Correction 2
                            //Replace with the tag of the found word
                            if (suggestions.Length == 1 //very sure that there is a mistake because only one suggestion
                                && inputWordIsDifferentFromDictionary
                                && typeIsEqual == false
                               )
                            {
                                string tag = (suggestions[0].Type == BejoWordType.Noun) ? "N" : "ADJ";
                                sentence[i].POSTaggedWord.setTag(tag);

                                corrected++;
                            }
                        }
                    }
                }
            }
        }
    }
}

//CC Coordinating conjunction
//CD Cardinal number
//DT Determiner
//EX Existential there
//FW Foreign word
//IN Preposition or subordinating conjunction
//JJ Adjective
//JJR Adjective, comparative
//JJS Adjective, superlative
//LS List item marker
//MD Modal
//NN Noun, singular or mass
//NNS Noun, plural
//NNP Proper noun, singular
//NNPS Proper noun, plural
//PDT Predeterminer
//POS Possessive ending
//PRP Personal pronoun
//PRP$ Possessive pronoun
//RB Adverb
//RBR Adverb, comparative
//RBS Adverb, superlative
//RP Particle
//SYM Symbol
//TO to
//UH Interjection
//VB Verb, base form
//VBD Verb, past tense
//VBG Verb, gerund or present participle
//VBN Verb, past participle
//VBP Verb, non­3rd person singular present
//VBZ Verb, 3rd person singular present
//WDT Wh­determiner
//WP Wh­pronoun
//WP$ Possessive wh­pronoun
//WRB Wh­adverb
