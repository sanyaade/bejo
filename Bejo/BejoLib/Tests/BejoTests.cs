﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.Threading.Tasks;

using BejoLib;

namespace BejoLib
{
    public static class BejoTests
    {
        public static BejoTest[] RunAllTests(bool isBasicMode)
        {

            BejoTest[] allTests = (from d in BejoDocumentDB.GetAllDocuments()
                                   select new BejoTest(d)).ToArray();

            if (isBasicMode)
                allTests = allTests.Where(x => !x.IsExtendedMode).ToArray(); //get only the tests for basic mode

            return RunTests(allTests, isBasicMode);
        }

        private static BejoTest[] RunTests(BejoTest[] tests,bool isBasicMode)
        { 
            int failedTests = 0;

            Parallel.For(0, tests.Length,
            i =>
            {
                BejoTest t = tests[i];

                BejoText text;

                text = Correction.Execute(t.WrongText, isBasicMode);
                tests[i].BejoCorrectedText = text.GetModifiedText();
                tests[i].POStext = "";

            });

            foreach (BejoTest t in tests)
            {
                bool ok = t.BejoCorrectedText == t.TrueText;

                if (!ok) Interlocked.Increment(ref failedTests);

                ////if (!ok)
                //{
                //    Console.WriteLine("----------------------------------");
                //    Console.Write(t.DocumentID + " |" + t.BejoCorrectedText + "|");
                //    if (ok)
                //        Console.WriteLine("T");
                //    else
                //    {
                //        Console.WriteLine("F");
                //    }
                //    Console.WriteLine("----------------------------------");
                //}
            }

           // Console.WriteLine("Failed tests: " + failedTests + "/" + BejoDocumentDB.GetDocumentsCount());

            return tests;
        }

        public static BejoTest[] RunAbbreviationsTests(bool isBasicMode)
        {
            int[] ids = BejoDocumentDB.GetAbreviationRules();

            int allTests = ids.Length;

            List<BejoTest> testsAbb = new List<BejoTest>();

            for (int i = 1; i <= BejoDocumentDB.GetDocumentsCount(); i++)
            {
                if (ids.Contains(i))
                {
                    BejoTest t = new BejoTest(BejoDocumentDB.GetDocumentByID(i));

                    if (!(string.IsNullOrEmpty(t.WrongText)))
                        testsAbb.Add(t);
                }
            }

            return RunTests(testsAbb.ToArray(), isBasicMode);
        }
    }
}
