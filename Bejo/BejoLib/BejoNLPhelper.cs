﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BejoLib
{
    public static class BejoNLPhelper
    {
        public static bool StartsWithVowel(string input)
        {
            bool startsWithVowel =
                    input.StartsWith("A", StringComparison.OrdinalIgnoreCase) || input.StartsWith("à", StringComparison.OrdinalIgnoreCase) || input.StartsWith("â", StringComparison.OrdinalIgnoreCase) ||
                    input.StartsWith("O", StringComparison.OrdinalIgnoreCase) || input.StartsWith("ô", StringComparison.OrdinalIgnoreCase) ||
                    input.StartsWith("U", StringComparison.OrdinalIgnoreCase) || input.StartsWith("ù", StringComparison.OrdinalIgnoreCase) || input.StartsWith("û", StringComparison.OrdinalIgnoreCase) ||
                    input.StartsWith("E", StringComparison.OrdinalIgnoreCase) || input.StartsWith("é", StringComparison.OrdinalIgnoreCase) || input.StartsWith("è", StringComparison.OrdinalIgnoreCase) || input.StartsWith("ê", StringComparison.OrdinalIgnoreCase) ||
                    input.StartsWith("I", StringComparison.OrdinalIgnoreCase) || input.StartsWith("Î", StringComparison.OrdinalIgnoreCase);

            return startsWithVowel;                  
        }

        public static bool IsVerbEtre(string verb)
        {
            bool isVerbEtre = verb == "suis" || verb == "es" || verb == "est" || verb == "sommes" || verb == "êtes" || verb == "sont";

            return isVerbEtre;
        }

        public static bool StartsWithVowelOrMuteH(string input)
        {
            return input.StartsWith("H", StringComparison.OrdinalIgnoreCase) || StartsWithVowel(input);
        }

        public static string ConvertToLowerAllE(string input)
        {
            string result = input.ToLower().Replace('é', 'e').Replace('è', 'e').Replace('ê', 'e');
            return result;
        }

        public static bool ContainsAnyE(string word)
        {
            return word.IndexOfAny(new char[] { 'e', 'é', 'è', 'ê' }) != -1;
        }
    }
}
