﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using java.io;
using java.util;
using edu.stanford.nlp.ling;
using edu.stanford.nlp.tagger.maxent;

namespace BejoLib
{
    public class BejoWord
    {
        string originalWord;

        public BejoWord(TaggedWord word)
        {
            POSTaggedWord = word;
            originalWord = POSTaggedWord.value();
        }

        /// <summary>
        /// This information is filled by the tagger
        /// </summary>
        public TaggedWord POSTaggedWord;

        /// <summary>
        /// Words that match the word used by the user (but not fully)
        /// </summary>
        //public BejoDictionaryWord[] MatchingWords;

        /// <summary>
        /// Contains one or more solutions to a mistake 
        /// </summary>
        public BejoDictionaryWord[] CorrectionWords;

        public string OriginalWord
        {
           get
            {
                return originalWord;
            }
        }

        /// <summary>
        /// The word/text that best fixes a mistake detected in thext
        /// </summary>
        public string CurrentBestCorrection
        {
            get; set;
        }

        public int CorrectionPosition
        {
            get
            {
                return POSTaggedWord.beginPosition();
            }
        }

        public int CorrectionLength
        {
            get
            {
                return POSTaggedWord.endPosition() - POSTaggedWord.beginPosition();
            }
        }

        public string LastCorrectionRuleApplied;

        public bool IsCorrected
        {
            get
            {
                return !string.IsNullOrEmpty(CurrentBestCorrection);
            }
        }
    }
}
