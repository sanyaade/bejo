﻿using System;
using System.IO;

namespace RidoCode.LogManager
{
    public sealed class FileRoller
    {
        static public string ResolveOrCreateRollingFile(string pathToReplace)
        {
            string fileName = ReplacePattern(pathToReplace);

            FileInfo fi = new FileInfo(
                Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName));

            DirectoryInfo di = new DirectoryInfo(fi.DirectoryName);

            if (!di.Exists)
            {
                di.Create();
            }

            if (!fi.Exists)
            {
                fi.CreateText().Close();
            }
            return fi.FullName;
        }

        static public string ReplacePattern(string data)
        {
            string pattern = "";

            int start = data.IndexOf("{");
            int end = data.IndexOf("}");

            if ((start > 0) && (end > 0))
            {
                pattern = data.Substring(start + 1, end - start - 1);
            }

            if (pattern.Length > 0)
            {
                return data.Replace("{" + pattern + "}",
                    DateTime.Now.ToString(pattern));
            }
            else
            {
                return data;
            }
        }
    }
}
