﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

//using System.Windows.Forms;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using System.Threading;
using System.Threading.Tasks;

using System.Xml;

namespace BejoLib
{
    public class BejoDictionary
    {
        public static List<BejoDictionaryWord> BejoWordDictionary = null;

        /// <summary>
        /// Helper method, used just once
        /// </summary>
        //public static void WriteDictioanry(string fullPathSerializationPath)
        //{
        //    //Binary serialization
        //    int nsize = 5000;

        //    List<List<BejoDictionaryWord>> splitted = BejoLib.BejoDictionary.splitList(BejoLib.BejoDictionary.BejoWordDictionary, nsize);

        //    List<byte[]> serializedObjects = new List<byte[]>();

        //    foreach (List<BejoDictionaryWord> part in splitted)
        //    {
        //        serializedObjects.Add(BejoLib.BejoDictionary.SerializeObject(part));
        //    }

        //    FileStream stream = File.Create(fullPathSerializationPath);
        //    var formatter = new BinaryFormatter();
        //    formatter.Serialize(stream, serializedObjects);
        //    stream.Close();
        //}

        //public static void Load(string path)
        //{
        //    FileStream stream = File.OpenRead(path);

        //    var formatter = new BinaryFormatter();

        //    List<byte[]> serizlizedObjects = (List<byte[]>)formatter.Deserialize(stream);

        //    List<BejoDictionaryWord>[] test = new List<BejoDictionaryWord>[serizlizedObjects.Count];

        //    //map
        //    Parallel.For(0, serizlizedObjects.Count,
        //    index =>
        //    {
        //        test[index] = (List<BejoDictionaryWord>)BejoLib.BejoDictionary.DeserializeObject(serizlizedObjects[index]);
        //    });

        //    //reduce
        //    BejoLib.BejoDictionary.BejoWordDictionary = test.SelectMany(x => x).ToList();
            
        //    stream.Close();
        //}

        //private static byte[] SerializeObject(object o)
        //{
        //    if (!o.GetType().IsSerializable)
        //    {
        //        return null;
        //    }

        //    using (MemoryStream stream = new MemoryStream())
        //    {
        //        new BinaryFormatter().Serialize(stream, o);
        //        //return Convert.ToBase64String(stream.ToArray());
        //        return stream.ToArray();
        //    }
        //}

        //private static object DeserializeObject(byte[] bytes)
        //{
        //    //byte[] bytes = Convert.FromBase64String(str);

        //    using (MemoryStream stream = new MemoryStream(bytes))
        //    {
        //        return new BinaryFormatter().Deserialize(stream);
        //    }
        //}

        //private static List<List<BejoDictionaryWord>> splitList(List<BejoDictionaryWord> locations, int nSize)
        //{
        //    List<List<BejoDictionaryWord>> list = new List<List<BejoDictionaryWord>>();

        //    for (int i = 0; i < locations.Count; i += nSize)
        //    {
        //        list.Add(locations.GetRange(i, Math.Min(nSize, locations.Count - i)));
        //    }

        //    return list;
        //}

        public static void ImportLefff(string filename)
        {
            //string[] lines = { "voiture	v	voiturer	PS13s" };
            string[] lines = { };

            string contents = File.ReadAllText(filename);

            string[] current_file_lines = contents.Split(new string[] { "\n" }, StringSplitOptions.None);

            lines = lines.Concat(current_file_lines).ToArray();

            //string[] test = lines[0].Split(new char[] { ' ', '\t' });

            var query = from c in lines.AsParallel()

                        let tabs = c.Split(new char[] { ' ', '\t' })

                        where tabs.Length > 3
                        where tabs[1].Length > 0
                        where tabs[3].Length > 0
                        where tabs[0].IndexOf("'") == -1 //ex: grand'	adj	grand	s

                        //we skip verbs,adverbs and NP for now
                        where (tabs[1].Equals("nc") || tabs[1].Equals("adj")) && !tabs[0][0].Equals('+') && !tabs[0][0].Equals('-') && !tabs[0][0].Equals('@')

                        let isNoun = (tabs[1].Equals("nc"))
                        let isAdj = (tabs[1].Equals("adj"))

                        let isAdjK = tabs[3][0].Equals('K') //K Participe Passé à valeur d'adjectif

                        let isMale = (tabs[3][0].Equals('m') || (isAdj && isAdjK && tabs[3].Length > 1 && tabs[3][1].Equals('m')))
                        let isFemale = (tabs[3][0].Equals('f') || (isAdj && isAdjK && tabs[3].Length > 1 && tabs[3][1].Equals('f')))

                        let isSingular = tabs[3][0].Equals('s') || (tabs[3].Length > 1 && tabs[3][1].Equals('s')) || (tabs[3].Length > 2 && isAdj && isAdjK && tabs[3][2].Equals('s'))
                        let isPlural = tabs[3][0].Equals('p') || (tabs[3].Length > 1 && tabs[3][1].Equals('p')) || (tabs[3].Length > 2 && isAdj && isAdjK && tabs[3][2].Equals('p'))
                        
                        //can generate invariant because it did not detect something
                        select new BejoDictionaryWord 
                        { 
                            Content = tabs[0], 
                            Type = (isNoun) ? BejoWordType.Noun : (isAdj) ? BejoWordType.Adjective : BejoWordType.NounOrAdjective, 
                            Gender = (isMale) ? BejoGender.Male : (isFemale) ? BejoGender.Female : BejoGender.Invariant, 
                            MainForm = tabs[2], 
                            Number = (isSingular) ? BejoNumber.Singular : (isPlural) ? BejoNumber.Plural : BejoNumber.Invariant,
                            IsParticipePasseAdjectif = isAdjK,
                            ContainsAnyE = BejoNLPhelper.ContainsAnyE(tabs[0]),
                        };

            try
            {
                BejoDictionary.BejoWordDictionary = query.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            FixDictionary();
        }

        private static void FixDictionary()
        {
            //fix: livre , mois
            Parallel.For(0, BejoDictionary.BejoWordDictionary.Count,
            i =>
            {
                //TODO: livre f. s. means "pound"
                if (BejoDictionary.BejoWordDictionary[i].Gender == BejoGender.Invariant && (BejoDictionary.BejoWordDictionary[i].Content == "livre" || BejoDictionary.BejoWordDictionary[i].Content == "livres"))
                {
                    BejoDictionaryWord w = BejoDictionary.BejoWordDictionary[i];
                    w.Gender = BejoGender.Male;
                    BejoDictionary.BejoWordDictionary[i] = w;
                }
                else
                    if (BejoDictionary.BejoWordDictionary[i].Gender == BejoGender.Male && BejoDictionary.BejoWordDictionary[i].Content == "mois" && BejoDictionary.BejoWordDictionary[i].Number == BejoNumber.Plural)
                {
                    BejoDictionaryWord w = BejoDictionary.BejoWordDictionary[i];
                    w.Number = BejoNumber.Singular;
                    BejoDictionary.BejoWordDictionary[i] = w;
                }
            });

            //remove extra instances of: mois, sol
            BejoDictionary.BejoWordDictionary = BejoDictionary.BejoWordDictionary.AsParallel().Where(x =>
                (!(x.Content == "mois" && x.Number == BejoNumber.Invariant)) &&
                (!(x.Content == "sol" && x.Number == BejoNumber.Plural)) &&
                (!(x.Content == "analysé" && x.Gender == BejoGender.Male)) &&
                (!(x.Content == "fois" && x.Number == BejoNumber.Plural)) &&
                (!(x.Content == "porté" && x.Gender == BejoGender.Male)) 

                //(!(x.Content == "fille" && x.Type == BejoWordType.Adjective)) &&
                //(!(x.Content == "arrivée" && x.Type == BejoWordType.Adjective))
                ).ToList();

            //add words:

            //video
            BejoDictionaryWord wVideo = new BejoDictionaryWord();

            wVideo.Gender = BejoGender.Invariant;
            wVideo.Number = BejoNumber.Invariant; 
            wVideo.Type = BejoWordType.Adjective;
            wVideo.IsParticipePasseAdjectif = false;
            wVideo.MainForm = "vidéo";
            wVideo.Content = "vidéo";
            wVideo.ContainsAnyE = true;

            BejoDictionary.BejoWordDictionary.Add(wVideo);

            //next
        }
    }
}
